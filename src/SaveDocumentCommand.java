public class SaveDocumentCommand implements Command {
    Document document;
    DocumentStorage storage;


    public SaveDocumentCommand(Document document, DocumentStorage storage) {
        this.document = document;
        this.storage = storage;
    }

    @Override
    public void execute() {
        storage.addElement(document);
    }

    @Override
    public void doCompensation() {
        storage.deleteElement(document);
    }


}
