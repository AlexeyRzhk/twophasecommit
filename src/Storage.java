import java.util.List;

public interface Storage<T> {

    void addElement(T element);

    void deleteElement(T element);

    T getElement(String key);

    boolean prepare();

    List<T> getElements();

}
