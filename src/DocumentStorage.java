import java.util.ArrayList;
import java.util.List;

public class DocumentStorage implements Storage<Document> {

    ArrayList<Document> storage = new ArrayList<>();
    private static int i = 0;


    @Override
    public void addElement(Document element) {
        if(!storage.contains(element)) {
            storage.add(element);
        }
    }

    @Override
    public void deleteElement(Document element) {
        storage.remove(element);
    }

    @Override
    public Document getElement(String key) {
        for(Document doc: storage){
            if(doc.getDocumentNumber().equals(key)){
                return doc;
            }
        }
        return null;
    }

    @Override
    public boolean prepare() {
        i++;
        if(i % 4 == 0) {
            return false;
        }
        else {
            return  true;
        }
    }

    @Override
    public List<Document> getElements() {
        return storage;
    }
}
