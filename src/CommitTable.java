import java.util.ArrayList;
import java.util.List;

public class CommitTable {
    private static ArrayList<Pair> table = new ArrayList<>();


    public static void addCommand(List<Command> commands){
        table.add(new Pair(commands));
    }

    public static List<Command> getCommand(){
        for(Pair pair: table){
            if (!pair.state){
                return pair.getCommand();
            }
        }
        return null;
    }

    public static void fixCommit(List<Command> command){
        for(int i = 0;i < table.size();i++){
            if (table.get(i).getCommand().equals(command)){
                table.get(i).setState(true);
            }
        }
    }

    public static List<Pair> getCommands(){
        return table;
    }

}
