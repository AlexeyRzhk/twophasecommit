
import java.util.List;

public class Pair {
    List<Command> commands;
    Boolean state;


    public Pair(List<Command> commands) {
        this.commands = commands;
        state = false;
    }

    public List<Command> getCommand() {
        return commands;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }
}
