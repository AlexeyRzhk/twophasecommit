import java.util.ArrayList;
import java.util.List;

public class PictureStorage implements Storage<Picture> {
    static ArrayList<Picture> storage = new ArrayList<>();
    private static int i = 0;

    @Override
    public void addElement(Picture element) {
        if(!storage.contains(element)) {
            storage.add(element);
        }
    }

    @Override
    public void deleteElement(Picture element) {
        storage.remove(element);
    }

    @Override
    public Picture getElement(String key) {
        for(Picture pic: storage){
            if(pic.getPictureNumber().equals(key)){
                return pic;
            }
        }
        return null;
    }

    @Override
    public boolean prepare() {
        i++;
        if(i % 3 == 0) {
            return false;
        }
        else {
            return  true;
        }
    }

    @Override
    public List<Picture> getElements() {
        return storage;
    }
}
