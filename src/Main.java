import java.util.ArrayList;

public class Main {

    private static DocumentStorage dStorage = new DocumentStorage();
    private static PictureStorage pStorage = new PictureStorage();
    private static Watcher watcher = new Watcher();

    public static void main(String[] args) {
        for(int i = 0;i < 3;i++) {
            ArrayList<Command> commands = new ArrayList<>();
            Document document = new Document();
            Picture picture = new Picture();
            document.addPicture(picture);
            commands.add(new SaveDocumentCommand(document, dStorage));
            commands.add(new SavePictureCommand(picture, pStorage));
            CommitTable.addCommand(commands);
        }


        for(int i = 0;i < 3;i++) {
            watcher.getCommand();
            if (watcher.prepare(dStorage)) {
                watcher.execute();

                if (watcher.prepare(pStorage)) {
                    watcher.execute();
                    watcher.commit();
                } else {
                    watcher.doRollback();
                }
            }
            watcher.refresh();
            print();
        }

    }


    public static void print(){
        for (Document doc: dStorage.getElements()){
            System.out.println(doc);
        }

        System.out.println();

        for(Picture pic: pStorage.getElements()){
            System.out.println(pic);
        }

        System.out.println();

        for(Pair pair: CommitTable.getCommands()){
            System.out.println(pair.getState());
        }
        System.out.println();
    }



}
