import java.util.ArrayList;
import java.util.Objects;

public class Document {
    private static int number;
    private String documentNumber;
    private String title;
    private ArrayList<Picture> pictures = new ArrayList<>();

    public Document() {
        documentNumber = "" + number;
        title = "document#" + number;
        number++;
    }

    @Override
    public String toString() {
        return "Document{" +
                "title='" + title + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return Objects.equals(documentNumber, document.documentNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(documentNumber);
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public ArrayList<Picture> getPictures() {
        return pictures;
    }

    public void addPicture(Picture picture){
        pictures.add(picture);
    }
}
