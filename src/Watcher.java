
import java.util.List;

public class Watcher {



    private int curCommand;


    private List<Command> command;



    public boolean prepare(Storage storage){
        return storage.prepare();
    }

    public void commit(){
        CommitTable.fixCommit(command);
    }

    public void doRollback(){
        for(int i = curCommand - 1;i >= 0; i--){
            command.get(i).doCompensation();
            curCommand--;
        }
    }

    public void getCommand(){
        command =  CommitTable.getCommand();
    }

    public boolean execute(){
        command.get(curCommand).execute();
        curCommand++;
        if(curCommand < command.size()){
            return true;
        }
        else{
            return false;
        }
    }

    public void refresh(){
        curCommand = 0;
    }



}
