public class SavePictureCommand implements Command {
    private Picture picture;
    private PictureStorage storage;


    public SavePictureCommand(Picture picture, PictureStorage storage) {
        this.picture = picture;
        this.storage = storage;
    }

    @Override
    public void execute() {
        storage.addElement(picture);
    }

    @Override
    public void doCompensation() {
        storage.deleteElement(picture);
    }
}
