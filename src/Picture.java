import java.util.Objects;

public class Picture {
    private static int number;
    private String pictureNumber;
    private String title;


    public Picture() {
        pictureNumber = "" + number;
        title = "picture#" + number;
        number++;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Picture picture = (Picture) o;
        return Objects.equals(pictureNumber, picture.pictureNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pictureNumber);
    }


    public String getPictureNumber() {
        return pictureNumber;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "title='" + title + '\'' +
                '}';
    }
}
